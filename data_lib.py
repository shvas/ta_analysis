
from kucoin_sam import client
import pandas as pd
from datetime import datetime,date, timedelta
from pathlib import Path

def build_data_set(
    file_path
) -> pd.DataFrame:
    """
    Upload the required csv data set from a specific folder
    :param fold: link to the directory containing the file we want to upload
    :param file_nam: name of the file we want to upload
    :return:
    """
    # file_path = Path(f'{fold}').joinpath(f"{file_nam}.csv")
    df = pd.read_csv(file_path)
    return df

class kucoin_retriever_shv:
    """
        Class to retrieve historical data from Kucoin exchange. Does not require API keys or secret. Generates a client object
        from kucoin_sam module

        The output is a dataframe containing for each crypto pair: Open price, close price, high price, low price,
                                                                    volume(amount), volume(notional).
                                                                    Column names are the crypto pair + "_datatype"
                                                                    Can access only a specific data type or only data
                                                                    for 1 pair by simply using the regex function.

        :param crypto: currency pair we want to get data for, this can be multiple pairs and needs to be passed as list
        :param timeperiod: frequency of data, 1min, 3min, 5min, 15min, 30min, 1hour, 2hour,
                           4hour, 6hour, 8hour, 12hour, 1day, 1week
        :param start_date: date from which we want to retrieve data required to be in DateTime
        :param end_date: date to whcih we want to retrieve data required to be in DateTime
        :return:
        """

    def __init__(self,api_key,api_secret,passphrase,crypto,start_date,end_date):
        self.api_key = api_key
        self.api_secret = api_secret
        self.passphrase = passphrase
        self.shv_client = client.Client(self.api_key,self.api_secret,self.passphrase)
        self.crypto = crypto
        self.start_date = start_date
        self.end_date = end_date


    def get_kucoin_data(self, timeperiod):

        for pair in self.crypto:

            if pair == self.crypto[0]:
                print(self.crypto[0])
                sd = self.start_date
                dt = self.end_date
                data_df = pd.DataFrame(['Generating this to make the loop work'])
                while data_df.empty==False:

                    if dt == date.today():

                        data = self.shv_client.get_kline_data(pair,timeperiod,sd.strftime('%s'),dt.strftime('%s'))

                        data_total = pd.DataFrame(data, columns=['Time', f'Open_{pair}', f'High_{pair}', f'Low_{pair}',
                                                                 f'Close_{pair}',f'Vol_Amount_{pair}',
                                                                 f'Vol_notional{pair}'])
                        if data_total.empty == True:

                            data_df = data_total

                        else:

                            dt = pd.to_datetime(data_total['Time'], unit='s').iloc[-1]

                    else:

                        data = self.shv_client.get_kline_data(pair,timeperiod,sd.strftime('%s'),dt.strftime('%s'))

                        data_df = pd.DataFrame(data, columns=['Time', f'Open_{pair}', f'High_{pair}', f'Low_{pair}',
                                                              f'Close_{pair}',f'Vol_Amount_{pair}',
                                                                 f'Vol_notional{pair}'])

                        data_total = pd.concat([data_total,data_df])

                        dt = pd.to_datetime(data_total['Time'], unit='s').iloc[-1]

            else:

                sd = self.start_date
                dt = self.end_date
                data_df = pd.DataFrame(['Generating this to make the loop work'])

                while data_df.empty==False:
                    print(pair)

                    if dt == date.today():

                        data = self.shv_client.get_kline_data(pair,timeperiod,sd.strftime('%s'),dt.strftime('%s'))
                        data_df = pd.DataFrame(data, columns=['Time', f'Open_{pair}', f'High_{pair}', f'Low_{pair}',
                                                              f'Close_{pair}',f'Vol_Amount_{pair}',
                                                                 f'Vol_notional{pair}'])
                        if data_df.empty == True:

                            data_df = data_df

                        else:

                            dt = pd.to_datetime(data_df['Time'], unit='s').iloc[-1]




                    else:

                        data = self.shv_client.get_kline_data(pair,timeperiod,sd.strftime('%s'),dt.strftime('%s'))

                        data_df1 = pd.DataFrame(data, columns=['Time', f'Open_{pair}', f'High_{pair}', f'Low_{pair}',
                                                               f'Close_{pair}',f'Vol_Amount_{pair}',
                                                                 f'Vol_notional{pair}'])

                        data_df2 = pd.concat([data_df,data_df1])

                        if data_df1.empty == True:

                            data_df = data_df1

                        else:

                            dt = pd.to_datetime(data_df1['Time'], unit='s').iloc[-1]
            try:
                data_df2
                data_total = data_total.merge(data_df2, on='Time', how='outer')

            except NameError:
                data_total = data_total

        data_total['Time']= pd.to_datetime(data_total['Time'], unit='s')
        data_total = data_total.set_index('Time', drop=True)
        cols = list(data_total.columns)

        # data_total[cols] = data_total[cols].apply(pd.to_numeric, errors='coerce')
        data_total[cols] = data_total[cols].astype(float)

        return data_total

    def get_kucoin_daily(self):

        df = self.get_kucoin_data('1day')

        return df

    def get_kucoin_hourly(self):

        df = self.get_kucoin_data('1hour')

        return df
