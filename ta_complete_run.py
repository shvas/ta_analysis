import pandas as pd


def full_breath_indicators(prices, ta_obj):

    """Function to calculate all of the indicators together a well as signals from TA class.
    :param prices: dataframe containing the relavant price data for the asset
    :param ta_obj: object of TAClass ( class for technical analysis tools"""

    ta = ta_obj
    indicators_list = [ta.sma(), ta.exp_ma(asset_p='Close',tframe=5), ta.exp_ma(asset_p='Close',tframe=21), ta.rsi(),
                       ta.roc(), ta.bb(), ta.cci(), ta.macd(), ta.williams_r(), ta.stochastic_oscillator()]

    final_df = prices

    for ind in indicators_list:
        indicator = ind

        try:
            final_df = pd.merge(final_df, indicator, left_index=True, right_index=True)

        except:
            final_df = pd.merge(final_df, indicator.to_frame(), left_index=True, right_index=True)

    signals_list = [ta.sma_signal(final_df),ta.exp_mas_signal(final_df,5,21),ta.bbs_signal(final_df),
                    ta.cci_signal(final_df),ta.macd_signal(final_df),ta.roc_signal(final_df),
                    ta.rsi_signal(final_df),ta.sto_signal(final_df),ta.sma_cci_signal(final_df),
                    ta.sma_roc_signal(final_df),ta.sma_rsi_signal(final_df),ta.sma_sto_signal(final_df)]

    for sig in signals_list:
        signal = sig

        try:
            final_df = pd.merge(final_df, signal, on = "Date", left_index=True, right_index=True)

        except:
            final_df = pd.merge(final_df, signal.to_frame(), left_index=True, right_index=True)

    return final_df


def btest_data_build(prices, bt_obj, sig):

    """Function to calculate performance of strategies using a signal.
    :param prices: dataframe containing the relavant price data for the asset
    :param bt_obj: object of BTester (Backtesting class for strategies
    :param sig: signal used for the strategy expects a string"""

    btest = bt_obj
    prices = prices.copy(deep=True)

    sma_nc = btest.run_perf_no_comms(signal=sig)
    sma_c = btest.run_perf_w_comms(signal=sig)
    bnl = btest.buy_and_hold()
    final_df = pd.merge(prices, sma_nc, left_index=True, right_index=True)
    final_df = pd.merge(final_df, sma_c, left_index=True, right_index=True)
    final_df = pd.merge(final_df, bnl, left_index=True, right_index=True)

    return final_df

def btest_results(prices, bt_obj, strat_name, strat_d, strat_cum, rf=0.00):

    """Function yo build backtest results table
    :param prices: dataframe containing the relavant price data for the asset
    :param bt_obj: object of BTester (Backtesting class for strategies:param: strat_name: name of strategy expects a string
    :param strat_d: daily returns of strategy used on the signal expects a string
    :param strat_cum: cumulative returns of strategy used on the signal expects a string
    :param rf: risk-free rate used to calculate sharpe expects a float"""

    btest = bt_obj
    prices = prices.copy(deep=True)

    ann_ret = btest.ann_ret(prices, measure=strat_cum)
    ann_vol = btest.ann_vol(prices, measure=strat_d)
    ann_sharpe = btest.ann_sharpe(ann_ret, ann_vol, rf)
    table = btest.stats_table(ann_ret, ann_vol, ann_sharpe, strat_name)

    return table
