# importing libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path

# data exploration
# making date as index
def build_data_set(
    fold, file_nam
) -> pd.DataFrame:
    """
    Upload the required csv data set from a specific folder
    :param fold:
    :param file_nam:
    :return:
    """

    file_name = f"{fold}/{file_nam}.csv"
    file_path = Path("/Users/simonehv/").joinpath(file_name)
    df = pd.read_csv(file_path)
    return df

crypto_df = build_data_set("Desktop/python/crypto/input/","all_currencies")

crypto_df = crypto_df.set_index(["Date"])
crypto_df.index = pd.to_datetime(crypto_df.index)

# Bar graph showing each year closing price
crypto_df['Close'].resample('Y').mean().plot(kind='bar')

# Top 10 cryptocurrencies in 2018 Market Cap wise
ax = crypto_df.groupby(['Symbol'])['Market Cap'].last().sort_values(ascending=False).head(10).sort_values().plot(kind='barh')
ax.set_xlabel("Market cap (in billion USD)")
plt.title("Top 10 Currencies by Market Cap")


# Defining The top 5 cryptocurrencies
top_5_currency_names = crypto_df.groupby(['Symbol'])['Market Cap'].last().sort_values(ascending=False).head(5).index
data_top_5_currencies = crypto_df[crypto_df['Symbol'].isin(top_5_currency_names)]
data_top_5_currencies.head(5)

# Yearly Trend Charts
# Closing Price
ax = data_top_5_currencies.groupby(['Date', 'Symbol'])['Close'].last().unstack().plot();
ax.set_ylabel("Price per 1 unit (in USD)");
plt.title("Price per unit of currency");
plt.show()