from src.mkt_lib import TAClass, BTester
import pandas as pd
from src.ta_complete_run import full_breath_indicators, btest_data_build, btest_results
from src.data_lib import kucoin_retriever_shv,build_data_set
from pathlib import Path
# import pandas_datareader as web
import matplotlib.pyplot as plt
import sys

# Load dataset
fold = '/Users/simonehv/Desktop/Python/crypto/output/'  #Here I define the folder where I will save the files
file_nam = 'kucoin_data_daily_new.csv'  #Here I define the name of the file including extension
file_path = Path().joinpath(f"{fold}/{file_nam}")

current_df = build_data_set(file_path).set_index('Time')# Load existing dataset
current_df.index.names = ['Date']

# Select asset for analysis
try:

    current_df = current_df.filter(regex = 'ADA-USDT')
    current_df.columns = current_df.columns.str.replace('_ADA-USDT','')

except:
    print('Asset not in Universe')
    sys.exit(1)

# current_df = pd.read_csv("Data//Stock-Technical-Analysis-Data.txt", index_col='Date', parse_dates=True)
current_df = current_df.reset_index()

# Calculate signals for previously selected asset
ta = TAClass(current_df, 'Close')
df = full_breath_indicators(current_df, ta)
signals = [col for col in df.columns if 'sig' in col]

# calculate backtest for a specific signal
btest = BTester(df, 'Close')

signal = 'sma_sto_sig'
final_df = btest_data_build(df,btest, signal)

# Prepare results table for buy and hold strat
final_btest = BTester(final_df, 'Close')
table = btest_results(final_df,final_btest, 'buy_and_hold','buy_and_hold_strat_daily_perf','buy_and_hold_strat_perf_cum')

# Prepare results table for signal strategy without commissions
table_1 = btest_results(final_df,final_btest,f'{signal}_strategy',f'{signal}_strat_daily_perf',f'{signal}_strat_perf_cum')

# Prepare results table for signal strategy with commissions
table_2 = btest_results(final_df,final_btest,f'{signal}_strategy_w_comms',f'{signal}_strat_daily_perf_comms',f'{signal}_strat_perf_comms_cum')

# Join all tables and show final result + charts

dfs = [table,table_1,table_2]
final_table = dfs[0].join(dfs[1:])
pd.set_option('display.max_columns', None)
print(final_table)

# final_df['sma_sig_strat_perf'].plot(marker='o', linestyle='solid')
final_df.plot(y=[f'{signal}_strat_perf_cum', f'{signal}_strat_perf_comms_cum', 'buy_and_hold_strat_perf_cum'])
plt.title('SMA (5) strategy (with and without commissions) vs Buy & Hold')
plt.legend(loc='upper left')
plt.show()

