import pandas as pd
import numpy as np

class TAClass:

    """This is a class to calculate technical indicators based on asset prices.
    Available are also methods to define signals and sample strategies
    Lagging INdicators: SMA, EMA, BB, PSR
    Leading indicators: Average Directional MOvement Index, CCI, MACD, ROC, RSI, STO, Williams"""

    def __init__(self, df, asset_p):

        """Init function to initiate the class.
        :param df: DataFrame containing the relevant price data
        :param asset_p: string representing the name of the column containing prices timeseries"""

        self.df = df
        self.asset_p = asset_p

    def sma(self, prices=None, asset_p=None, n=5):

        """IMPLE MOVING AVERAGE
        :param asset_p: column name for the series of closing prices for asset
        :param n: window to calculate simple moving average for
        :param prices: Dataframe containing price data"""

        asset_p = self.asset_p if asset_p is None else asset_p
        prices = self.df.reset_index() if prices is None else prices
        prices = prices.copy(deep=True)
        prices[f'sma_{n}'] = pd.Series(prices[asset_p]).rolling(window=n).mean()

        return prices[f'sma_{n}']

    def exp_ma(self, prices=None, asset_p=None, tframe=5):

        """MOVING AVERAGES CONVERGENCE/DIVERGENCE
        :param asset_p: column name for the series of closing prices for asset
        :param tframe: timeframe for the window expressed as number of obs. Defaults to 5
        :param prices: Dataframe containing price data"""
        
        asset_p = self.asset_p if asset_p is None else asset_p
        prices = self.df.reset_index() if prices is None else prices
        prices = prices.copy(deep=True)
        # Get the exp_ma of the closing price
        prices[f'exp_ma_{tframe}'] = prices[asset_p].ewm(span=tframe, adjust=False, min_periods=tframe).mean()

        return prices[f'exp_ma_{tframe}']

    def rsi(self, prices=None, asset_p=None, lag = 14):
        
        """RELATIVE STRENGTH INDEX
        :param asset_p: column name for the series of closing prices for asset as string
        :param lag: lag to calculate RSI. Defaults to standard of 14
        :param prices: Dataframe containing price data"""

        asset_p = self.asset_p if asset_p is None else asset_p
        prices = self.df.reset_index() if prices is None else prices
        prices = prices.copy(deep=True)
        delta = prices[asset_p].diff()
        up = delta.clip(lower=0)
        down = -1*delta.clip(upper=0)
        exp_ma_up = up.ewm(com=(lag-1), adjust=False).mean()
        exp_ma_down = down.ewm(com=(lag-1), adjust=False).mean()
        rs_series = exp_ma_up/exp_ma_down
        prices['rsi'] = 100 - (100 / (1 + rs_series))

        return prices['rsi']

    def bb(self, prices=None, asset_p=None, n=20):
        """ BOLLINGER BANDS
        :param asset_p: column name for the series of closing prices for asset
        :param n: window to calculate BB. Defaults to standard of 20
        :param prices: Dataframe containing price data"""

        asset_p = self.asset_p if asset_p is None else asset_p
        prices = self.df.reset_index() if prices is None else prices
        prices = prices.copy(deep=True)
        prices['mid_band'] = pd.Series(prices[asset_p]).rolling(window=n).mean()
        prices['upper_band'] = prices['mid_band'] + 2*(pd.Series(prices[asset_p]).rolling(window=n).std().values)
        prices['lower_band'] = prices['mid_band'] - 2*(pd.Series(prices[asset_p]).rolling(window=n).std().values)

        return prices[['lower_band','mid_band','upper_band']]

    def roc(self, prices=None, asset_p=None, n=21):
        """RATE OF CHANGE
        :param asset_p: column name for the series of closing prices for asset as string
        :param n: window to calculate BB. Defaults to standard of 20
        :param prices: Dataframe containing price data"""

        asset_p = self.asset_p if asset_p is None else asset_p
        prices = self.df.reset_index() if prices is None else prices
        prices = prices.copy(deep=True)
        prices['roc'] = prices[asset_p].pct_change(periods=n)

        return prices['roc']

    def cci(self, prices=None, asset_p=None, hi = 'High', lo = 'Low',n=20,c=0.0151):
        """COMMODITY CHANNEL INDEX
        :param prices: Dataframe containing price data
        :param asset_p: column name for the series of closing prices for asset as string
        :param hi: column name for the series of high prices for asset
        :param lo: column name for the series of low prices for asset
        :param n: window to calculate CCI. Defaults to standard of 20
        :param c: window to calculate coefficient. Defaults to standard of 0.0151"""

        asset_p = self.asset_p if asset_p is None else asset_p

        prices = self.df.reset_index() if prices is None else prices
        prices = prices.copy(deep=True)

        prices['tp'] = (prices[hi] + prices[lo] + prices[asset_p]) / 3
        prices['sma'] = pd.Series(prices[asset_p]).rolling(window=n).mean().values
        prices['diff'] = prices.sma - prices.tp
        prices['mad'] = pd.Series(prices['diff']).rolling(window=n).mean().values
        prices['cci'] = (prices['tp'] - prices['sma']) / (c * prices['mad'])

        return prices['cci']

    def psar(self, asset_p=None, date = 'Date', hi = 'High', lo = 'Low', iaf = 0.02, maxaf = 0.2):

        """Extreme Up = HIghest high in current uptrend
        Extreme low= Lowest low in current uptrend
        AF(Up) = If new high in EP(U) -> current AF (U) = Previous AF(U)+0.02 else Current AF(U) = Previous (AF(U)
        AF(Down) = If new low in EP(Down) -> current AF (D) = Previous AF(Down)+0.02 else Current AF(D) = Previous (AF(D)
        Current SAR(U) = Previosu SAR(Up) + Previous AF*(prev EP(U)-Prev SAR(U)
        Current SAR(D) = Previosu SAR(D) - Previous AF*(prev EP(D)-Prev SAR(D)"""

        asset_p = self.asset_p if asset_p is None else asset_p
        df = self.df.reset_index()
        length = len(df)
        dates = list(df[date])
        high = list(df[hi])
        low = list(df[lo])
        close = list(df[asset_p])
        psar = close[0:len(close)]

        psarbull = [None] * length
        psarbear = [None] * length
        bull = True
        af = iaf
        ep = low[0]
        hp = high[0]
        lp = low[0]
        for i in range(2,length):
            if bull:
                psar[i] = psar[i - 1] + af * (hp - psar[i - 1])
            else:
                psar[i] = psar[i - 1] + af * (lp - psar[i - 1])
            reverse = False
            if bull:
                if low[i] < psar[i]:
                    bull = False
                    reverse = True
                    psar[i] = hp
                    lp = low[i]
                    af = iaf
            else:
                if high[i] > psar[i]:
                    bull = True
                    reverse = True
                    psar[i] = lp
                    hp = high[i]
                    af = iaf
            if not reverse:
                if bull:
                    if high[i] > hp:
                        hp = high[i]
                        af = min(af + iaf, maxaf)
                    if low[i - 1] < psar[i]:
                        psar[i] = low[i - 1]
                    if low[i - 2] < psar[i]:
                        psar[i] = low[i - 2]
                else:
                    if low[i] < lp:
                        lp = low[i]
                        af = min(af + iaf, maxaf)
                    if high[i - 1] > psar[i]:
                        psar[i] = high[i - 1]
                    if high[i - 2] > psar[i]:
                        psar[i] = high[i - 2]
            if bull:
                psarbull[i] = psar[i]
            else:
                psarbear[i] = psar[i]
        return {"dates":dates, "high":high, "low":low, "close":close, "psar":psar, "psarbear":psarbear, "psarbull":psarbull}

    def macd(self, prices=None, asset_p=None, short_tframe=12, long_tframe=26):

        """MOVING AVERAGES CONVERGENCE/DIVERGENCE
        :param asset_p: series of closing prices for asset
        :param short_tframe: timeframe for the shorter window expressed as number of obs. Defaults to 12
        :param long_tframe: timeframe for the longer window expressed as number of obs. Defaults to 26"""

        asset_p = self.asset_p if asset_p is None else asset_p
        prices = self.df.reset_index() if prices is None else prices
        prices = prices.copy(deep=True)

        # Get the short Exp_ma of the closing price
        exp_ma_st = prices[asset_p].ewm(span=short_tframe, adjust=False, min_periods=short_tframe).mean()
        # Get the long exp ma of the closing price
        exp_ma_lt = prices[asset_p].ewm(span=long_tframe, adjust=False, min_periods=long_tframe).mean()

        # Subtract the 26-day exp ma from the 12-Day exp ma to get the MACD
        macd = exp_ma_st - exp_ma_lt

        # Get the 9-Day exp ma of the MACD for the Trigger line
        macd_signal = macd.ewm(span=9, adjust=False, min_periods=9).mean()

        # Calculate the difference between the MACD - Trigger for the Convergence/Divergence value
        macd_histogram = macd - macd_signal

        df_macd = pd.DataFrame(
            {'exp_ma_st': exp_ma_st,
             'exp_ma_lt': exp_ma_lt,
             'macd': macd,
             'macd_s': macd_signal,
             'macd_histogram': macd_histogram
             })

        return df_macd[['macd','macd_s','macd_histogram']]

    def stochastic_oscillator(self, prices=None):

        """STOCHASTIC OSCILLATOR
        :param prices: DataFrame containing High, Low and close prices for a security"""

        prices = self.df.reset_index() if prices is None else prices
        prices = prices.copy(deep=True)
        prices['14-high'] = prices['High'].rolling(14).max()
        prices['14-low'] = prices['Low'].rolling(14).min()
        prices['%K'] = (prices['Close'] - prices['14-low']) * 100 / (prices['14-high'] - prices['14-low'])
        prices['%D'] = prices['%K'].rolling(3).mean()
        prices['%D_slow'] = prices['%D'].rolling(3).mean()

        return prices[['%K','%D','%D_slow']]

    def williams_r(self, prices=None):

        """WILLIAMS %R
        :param prices: DataFrame containing High, Low and close prices for a security"""

        prices = self.df.reset_index() if prices is None else prices
        prices = prices.copy(deep=True)
        prices['14-high'] = prices['High'].rolling(14).max()
        prices['14-low'] = prices['Low'].rolling(14).min()
        prices['williams_r'] = (prices['14-high'] - prices['Close']) * (-100) / (prices['14-high'] - prices['14-low'])

        return prices[['14-high','14-low','williams_r']]

    def sma_signal(self, prices=None, n=5, asset_p=None):

        """SMA Crossover Signal
        :param prices: DataFrame containing High, Low and close prices for a security
        :param n: window to calculate simple moving average for
        :param asset_p: column name of prices tseries as string"""

        asset_p = self.asset_p if asset_p is None else asset_p

        prices = self.df.reset_index() if prices is None else prices
        prices = prices.copy(deep=True)
        # 3. Price Crossover Trading Signals
        # Previous Periods Data (avoid backtesting bias)

        prices[f'{asset_p}(-1)'] = prices[asset_p].shift(1)
        prices['sma(-1)'] = prices[f'sma_{n}'].shift(1)
        prices[f'{asset_p}(-2)'] = prices[asset_p].shift(2)
        prices['sma(-2)'] = prices[f'sma_{n}'].shift(2)

        # Generate Trading Signals (buy=1 , sell=-1, do nothing=0)
        prices['sma_sig'] = 0

        for i, r in enumerate(prices.iterrows()):
            if r[1][f'{asset_p}(-2)'] < r[1]['sma(-2)'] and r[1][f'{asset_p}(-1)'] > r[1]['sma(-1)']:
                sma_sig = 1
            elif r[1][f'{asset_p}(-2)'] > r[1]['sma(-2)'] and r[1][f'{asset_p}(-1)'] < r[1]['sma(-1)']:
                sma_sig = -1
            else:
                sma_sig = 0
            prices.iloc[i,prices.columns.get_loc('sma_sig')] = sma_sig

        return prices['sma_sig']

    def exp_mas_signal(self, prices=None, n=5, z=21):

        """EMA Double Crossover Signal
        :param prices: DataFrame containing High, Low and close prices for a security
        :param n: window to calculate short timeframe ema for
        :param z: window to calculate long timeframe ema for"""

        prices = self.df.reset_index() if prices is None else prices
        prices = prices.copy(deep=True)

        prices['exp_ma_st(-1)'] = prices[f'exp_ma_{n}'].shift(1)
        prices['exp_ma_lt(-1)'] = prices[f'exp_ma_{z}'].shift(1)
        prices['exp_ma_st(-2)'] = prices[f'exp_ma_{n}'].shift(2)
        prices['exp_ma_lt(-2)'] = prices[f'exp_ma_{z}'].shift(2)

        # Generate Trading Signals (buy=1 , sell=-1, do nothing=0)
        prices['exp_ma_sig'] = 0

        for i, r in enumerate(prices.iterrows()):
            if r[1]['exp_ma_st(-2)'] < r[1]['exp_ma_lt(-2)'] and r[1]['exp_ma_st(-1)'] > r[1]['exp_ma_lt(-1)']:
                exp_ma_sig = 1
            elif r[1]['exp_ma_st(-2)'] > r[1]['exp_ma_lt(-2)'] and r[1]['exp_ma_st(-1)'] < r[1]['exp_ma_lt(-1)']:
                exp_ma_sig = -1
            else:
                exp_ma_sig = 0
            prices.iloc[i,prices.columns.get_loc('exp_ma_sig')] = exp_ma_sig

        return prices['exp_ma_sig']

    def bbs_signal(self, prices=None, n=20, asset_p=None):

        """Bollinger Bands Crossover Signal
        :param prices: DataFrame containing High, Low and close prices for a security
        :param n: window to calculate short timeframe ema for
        :param n: window to calculate BB. Defaults to standard of 20
        :param asset_p: column name of prices tseries as string"""

        asset_p = self.asset_p if asset_p is None else asset_p

        prices = self.df.reset_index() if prices is None else prices
        prices = prices.copy(deep=True)
        prices[f'{asset_p}(-1)'] = prices[asset_p].shift(1)
        prices[f'{asset_p}(-2)'] = prices[asset_p].shift(2)
        prices['lower(-1)'] = prices['lower_band'].shift(1)
        prices['lower(-2)'] = prices['lower_band'].shift(2)
        prices['upper(-1)'] = prices['upper_band'].shift(1)
        prices['upper(-2)'] = prices['upper_band'].shift(2)

        # Generate Trading Signals (buy=1 , sell=-1, do nothing=0)
        prices['bb_sig'] = 0

        for i, r in enumerate(prices.iterrows()):
            if r[1][f'{asset_p}(-2)'] < r[1]['lower(-2)'] and r[1][f'{asset_p}(-1)'] > r[1]['lower(-1)']:
                bb_sig = 1
            elif r[1][f'{asset_p}(-2)'] < r[1]['upper(-2)'] and r[1][f'{asset_p}(-1)'] > r[1]['upper(-1)']:
                bb_sig = -1
            else:
                bb_sig = 0

            prices.iloc[i,prices.columns.get_loc('bb_sig')] = bb_sig

        return prices['bb_sig']

    def cci_signal(self, prices=None):

        """Commodity Channel Index Crossover Signal
        :param prices: DataFrame containing High, Low and close prices for a security"""

        prices = self.df.reset_index() if prices is None else prices
        prices = prices.copy(deep=True)
        prices['cci(-1)'] = prices['cci'].shift(1)
        prices['cci(-2)'] = prices['cci'].shift(2)

        # Generate Trading Signals (buy=1 , sell=-1, do nothing=0)
        prices['cci_sig'] = 0

        for i, r in enumerate(prices.iterrows()):
            if r[1]['cci(-2)'] < -100 and r[1]['cci(-1)'] > -100:
                cci_sig = 1
            elif r[1]['cci(-2)'] < 100 and r[1]['cci(-1)'] > 100:
                cci_sig = -1
            else:
                cci_sig = 0
            prices.iloc[i,prices.columns.get_loc('cci_sig')] = cci_sig

        return prices['cci_sig']

    def macd_signal(self, prices=None):

        """MACD Crossover signal
        :param prices: DataFrame containing High, Low and close prices for a security"""

        prices = self.df.reset_index() if prices is None else prices
        prices = prices.copy(deep=True)
        prices['macd(-1)'] = prices['macd'].shift(1)
        prices['macd(-2)'] = prices['macd'].shift(2)
        prices['macd_sig(-1)'] = prices['macd_s'].shift(1)
        prices['macd_sig(-2)'] = prices['macd_s'].shift(2)

        # Generate Trading Signals (buy=1 , sell=-1, do nothing=0)ƒ
        prices['macd_sig'] = 0

        for i, r in enumerate(prices.iterrows()):
            if r[1]['macd(-2)'] < r[1]['macd_sig(-2)'] and r[1]['macd(-1)'] > r[1]['macd_sig(-1)']:
                macd_sig = 1
            elif r[1]['macd(-2)'] > r[1]['macd_sig(-2)'] and r[1]['macd(-1)'] < r[1]['macd_sig(-1)']:
                macd_sig = -1
            else:
                macd_sig = 0
            prices.iloc[i,prices.columns.get_loc('macd_sig')] = macd_sig

        return prices['macd_sig']

    def roc_signal(self, prices=None):

        """ROC Crossover signal
        :param prices: DataFrame containing High, Low and close prices for a security"""

        prices = self.df.reset_index() if prices is None else prices
        prices = prices.copy(deep=True)
        prices['roc(-1)'] = prices['roc'].shift(1)
        prices['roc(-2)'] = prices['roc'].shift(2)

        # Generate Trading Signals (buy=1 , sell=-1, do nothing=0)
        prices['roc_sig'] = 0

        for i, r in enumerate(prices.iterrows()):
            if r[1]['roc(-2)'] < -10 and r[1]['roc(-1)'] > -10:
                roc_sig = 1
            elif r[1]['roc(-2)'] < 10 and r[1]['roc(-1)'] >10:
                roc_sig = -1
            else:
                roc_sig= 0
            prices.iloc[i,prices.columns.get_loc('roc_sig')] = roc_sig

        return prices['roc_sig']

    def rsi_signal(self, prices=None):

        """RELATIVE STRENGTH INDEX Bands Crossover Signal
        :param prices: DataFrame containing High, Low and close prices for a security"""

        prices = self.df.reset_index() if prices is None else prices
        prices = prices.copy(deep=True)
        prices['rsi(-1)'] = prices['rsi'].shift(1)
        prices['rsi(-2)'] = prices['rsi'].shift(2)

        # Generate Trading Signals (buy=1 , sell=-1, do nothing=0)
        prices['rsi_sig'] = 0

        for i, r in enumerate(prices.iterrows()):
            if r[1]['rsi(-2)'] < 30 and r[1]['rsi(-1)'] > 30:
                rsi_sig = 1
            elif r[1]['rsi(-2)'] < 70 and r[1]['rsi(-1)'] >70:
                rsi_sig = -1
            else:
                rsi_sig = 0
            prices.iloc[i, prices.columns.get_loc('rsi_sig')] = rsi_sig

        return prices['rsi_sig']

    def sto_signal(self, prices=None):

        """STOCHASTIC OSCILLATOR Bands Crossover Signal
        :param prices: DataFrame containing High, Low and close prices for a security"""

        prices = self.df.reset_index() if prices is None else prices
        prices = prices.copy(deep=True)
        prices['%D_slow(-1)'] = prices['%D_slow'].shift(1)
        prices['%D_slow(-2)'] = prices['%D_slow'].shift(2)

        # Generate Trading Signals (buy=1 , sell=-1, do nothing=0)
        prices['sto_sig'] = 0

        for i, r in enumerate(prices.iterrows()):
            if r[1]['%D_slow(-2)'] < 20 and r[1]['%D_slow(-1)'] > 20:
                sto_sig = 1
            elif r[1]['%D_slow(-2)'] < 80 and r[1]['%D_slow(-1)'] >80:
                sto_sig = -1
            else:
                sto_sig = 0
            prices.iloc[i, prices.columns.get_loc('sto_sig')] = sto_sig

        return prices['sto_sig']

    def sma_cci_signal(self, prices=None, n=5, asset_p=None):

        """SMA CCI Crossover signal
        :param prices: DataFrame containing High, Low and close prices for a security
        :param n: length of the simple moving average (sma) to be tested
        :param asset_p: column name of prices tseries as string"""

        asset_p = self.asset_p if asset_p is None else asset_p

        prices = self.df.reset_index() if prices is None else prices
        prices = prices.copy(deep=True)
        prices[f'{asset_p}(-1)'] = prices[asset_p].shift(1)
        prices[f'{asset_p}(-2)'] = prices[asset_p].shift(2)
        prices['cci(-1)'] = prices['cci'].shift(1)
        prices[f'sma_{n}_(-1)'] = prices[f'sma_{n}'].shift(1)
        prices[f'sma_{n}_(-2)'] = prices[f'sma_{n}'].shift(2)

        # Generate Trading Signals (buy=1 , sell=-1, do nothing=0)
        prices['sma_cci_sig'] = 0

        for i, r in enumerate(prices.iterrows()):
            if r[1][f'{asset_p}(-2)'] < r[1][f'sma_{n}_(-2)'] and r[1][f'{asset_p}(-1)'] > r[1][f'sma_{n}_(-1)'] and r[1]['cci(-1)']<-100:
                sma_cci_sig = 1
            elif r[1][f'{asset_p}(-2)'] > r[1][f'sma_{n}_(-2)'] and r[1][f'{asset_p}(-1)'] < r[1][f'sma_{n}_(-1)'] and r[1]['cci(-1)']>100:
                sma_cci_sig = -1
            else:
                sma_cci_sig = 0
            prices.iloc[i, prices.columns.get_loc('sma_cci_sig')] = sma_cci_sig

        return prices['sma_cci_sig']

    def sma_roc_signal(self, prices=None, n=5, asset_p=None):

        """SMA ROC Crossover signal
        :param prices: DataFrame containing High, Low and close prices for a security
        :param n: length of the simple moving average (sma) to be tested
        :param asset_p: column name of prices tseries as string"""

        asset_p = self.asset_p if asset_p is None else asset_p

        prices = self.df.reset_index() if prices is None else prices
        prices = prices.copy(deep=True)
        prices[f'{asset_p}(-1)'] = prices[asset_p].shift(1)
        prices[f'{asset_p}(-2)'] = prices[asset_p].shift(2)
        prices['roc(-1)'] = prices['roc'].shift(1)
        prices[f'sma_{n}_(-1)'] = prices[f'sma_{n}'].shift(1)
        prices[f'sma_{n}_(-2)'] = prices[f'sma_{n}'].shift(2)

        # Generate Trading Signals (buy=1 , sell=-1, do nothing=0)
        prices['sma_roc_sig'] = 0

        for i, r in enumerate(prices.iterrows()):
            if r[1][f'{asset_p}(-2)'] < r[1][f'sma_{n}_(-2)'] and r[1][f'{asset_p}(-1)'] > r[1][f'sma_{n}_(-1)'] and r[1]['roc(-1)']<-10:
                sma_roc_sig = 1
            elif r[1][f'{asset_p}(-2)'] > r[1][f'sma_{n}_(-2)'] and r[1][f'{asset_p}(-1)'] < r[1][f'sma_{n}_(-1)'] and r[1]['roc(-1)']>100:
                sma_roc_sig = -1
            else:
                sma_roc_sig = 0
            prices.iloc[i, prices.columns.get_loc('sma_roc_sig')] = sma_roc_sig

        return prices['sma_roc_sig']

    def sma_rsi_signal(self, prices=None, n=5, asset_p=None):

        """SMA RSI Crossover signal
        :param prices: DataFrame containing High, Low and close prices for a security
        :param n: length of the simple moving average (sma) to be tested
        :param asset_p: column name of prices tseries as string"""

        asset_p = self.asset_p if asset_p is None else asset_p

        prices = self.df.reset_index() if prices is None else prices
        prices = prices.copy(deep=True)
        prices[f'{asset_p}(-1)'] = prices[asset_p].shift(1)
        prices[f'{asset_p}(-2)'] = prices[asset_p].shift(2)
        prices['rsi(-1)'] = prices['rsi'].shift(1)
        prices[f'sma_{n}_(-1)'] = prices[f'sma_{n}'].shift(1)
        prices[f'sma_{n}_(-2)'] = prices[f'sma_{n}'].shift(2)

        # Generate Trading Signals (buy=1 , sell=-1, do nothing=0)
        prices['sma_rsi_sig'] = 0

        for i, r in enumerate(prices.iterrows()):
            if r[1][f'{asset_p}(-2)'] < r[1][f'sma_{n}_(-2)'] and r[1][f'{asset_p}(-1)'] > r[1][f'sma_{n}_(-1)'] and r[1]['rsi(-1)']<30:
                sma_rsi_sig = 1
            elif r[1][f'{asset_p}(-2)'] > r[1][f'sma_{n}_(-2)'] and r[1][f'{asset_p}(-1)'] < r[1][f'sma_{n}_(-1)'] and r[1]['rsi(-1)']>70:
                sma_rsi_sig = -1
            else:
                sma_rsi_sig = 0
            prices.iloc[i, prices.columns.get_loc('sma_rsi_sig')] = sma_rsi_sig

        return prices['sma_rsi_sig']

    def sma_sto_signal(self, prices=None, n=5, asset_p=None):

        """SMA STOCHASTIC OSCILLATOR Crossover signal
        :param prices: DataFrame containing High, Low and close prices for a security
        :param n: length of the simple moving average (sma) to be tested
        :param asset_p: column name of prices tseries as string"""

        asset_p = self.asset_p if asset_p is None else asset_p

        prices = self.df.reset_index() if prices is None else prices
        prices = prices.copy(deep=True)
        prices[f'{asset_p}(-1)'] = prices[asset_p].shift(1)
        prices[f'{asset_p}(-2)'] = prices[asset_p].shift(2)
        prices['%D_slow(-1)'] = prices['%D_slow'].shift(1)
        prices[f'sma_{n}_(-1)'] = prices[f'sma_{n}'].shift(1)
        prices[f'sma_{n}_(-2)'] = prices[f'sma_{n}'].shift(2)

        # Generate Trading Signals (buy=1 , sell=-1, do nothing=0)
        prices['sma_sto_sig'] = 0

        for i, r in enumerate(prices.iterrows()):
            if r[1][f'{asset_p}(-2)'] < r[1][f'sma_{n}_(-2)'] and r[1][f'{asset_p}(-1)'] > r[1][f'sma_{n}_(-1)'] and r[1]['%D_slow(-1)']<20:
                sma_sto_sig = 1
            elif r[1][f'{asset_p}(-2)'] > r[1][f'sma_{n}_(-2)'] and r[1][f'{asset_p}(-1)'] < r[1][f'sma_{n}_(-1)'] and r[1]['%D_slow(-1)']>80:
                sma_sto_sig = -1
            else:
                sma_sto_sig = 0
            prices.iloc[i, prices.columns.get_loc('sma_sto_sig')] = sma_sto_sig

        return prices['sma_sto_sig']

    def sma_wr_signal(self, prices=None, n=5, asset_p=None):

        """SMA William's %r Crossover signal
        :param prices: DataFrame containing High, Low and close prices for a security
        :param n: length of the simple moving average (sma) to be tested
        :param asset_p: column name of prices tseries"""

        asset_p = self.asset_p if asset_p is None else asset_p

        prices = self.df.reset_index() if prices is None else prices
        prices = prices.copy(deep=True)
        prices[f'{asset_p}(-1)'] = prices[asset_p].shift(1)
        prices[f'{asset_p}(-2)'] = prices[asset_p].shift(2)
        prices['williams_r(-1)'] = prices['williams_r'].shift(1)
        prices[f'sma_{n}_(-1)'] = prices[f'sma_{n}'].shift(1)
        prices[f'sma_{n}_(-2)'] = prices[f'sma_{n}'].shift(2)

        # Generate Trading Signals (buy=1 , sell=-1, do nothing=0)
        prices['sma_wr_sig'] = 0

        for i, r in enumerate(prices.iterrows()):
            if r[1][f'{asset_p}(-2)'] < r[1][f'sma_{n}_(-2)'] and r[1][f'{asset_p}(-1)'] > r[1][f'sma_{n}_(-1)'] and r[1]['williams_r(-1)']<-80:
                sma_wr_sig = 1
            elif r[1][f'{asset_p}(-2)'] > r[1][f'sma_{n}_(-2)'] and r[1][f'{asset_p}(-1)'] < r[1][f'sma_{n}_(-1)'] and r[1]['williams_rw(-1)']>-20:
                sma_wr_sig = -1
            else:
                sma_wr_sig = 0
            prices.iloc[i, prices.columns.get_loc('sma_wr_sig')] = sma_wr_sig

        return prices['sma_wr_sig']

    def long_only_strategy(self, prices, signal):

        """Method to create a trading strategy based on a signal
        :param prices: DataFrame containing High, Low and close prices for a security as well as signal
        :param signal: series containing a set of 1,0,-1 values indicating the signal"""

        prices = self.df.reset_index() if prices is None else prices
        prices = prices.copy(deep=True)
        prices[f"{signal}_strategy"] = 1

        for i, r in enumerate(prices.iterrows()):
            if r[1][signal] == 1:
                strategy_build = 1
            elif r[1][signal] == -1:
                strategy_build = 0
            else:
                strategy_build = prices[f"{signal}_strategy"][i - 1]
            prices.iloc[i, prices.columns.get_loc(f"{signal}_strategy")] = strategy_build

        return prices[f"{signal}_strategy"]


class BTester:

    """This is a class to calculate basic backtesting based on df w prices and signals
    It include a method to define positioning based on the signal as well as calculate cumulative perf
    for strategy with and without commissions and compare it with buy and hold."""

    def __init__(self, df, asset_p):
        self.df = df
        self.asset_p = asset_p

    def long_only_strategy(self, signal, prices=None):

        """Method to track a srategy positioning based on a signal
        :param prices: DataFrame containing High, Low and close prices for a security as well as signal
        :param signal: series containing a set of 1,0,-1 values indicating the signal"""

        prices = self.df.reset_index() if prices is None else prices
        prices = prices.copy(deep=True)
        prices[f"{signal}_strategy"] = 0
        strategy_build = 0
        for i, r in enumerate(prices.iterrows()):
            if r[1][signal] == 1:
                strategy_build = 1
            elif r[1][signal] == -1:
                strategy_build = 0
            else:
                if i == 0:
                    strategy_build = 0
                else:

                    strategy_build = prices[f"{signal}_strategy"][i-1]
            prices.iloc[i, prices.columns.get_loc(f"{signal}_strategy")] = strategy_build

        return prices[f"{signal}_strategy"].shift(1)

    def run_perf_no_comms(self, signal, prices=None, asset_p=None):

        """Method to calculate cumulative return of a strategy for a specific signal. Requires the signal input.
        It creates a lon only strategy using the previous method.
        :param prices: DataFrame containing High, Low and close prices for a security as well as signal
        :param asset_p: column name of prices tseries
        :param signal: series containing a set of 1,0,-1 values indicating the signal"""

        asset_p = self.asset_p if asset_p is None else asset_p

        prices = self.df.reset_index() if prices is None else prices
        prices = prices.copy(deep=True)

        prices['returns'] = prices[asset_p].pct_change()

        if f'{signal}_strategy' not in prices.columns:

            prices[f'{signal}_strategy'] = self.long_only_strategy(signal = signal)

        else:
            pass

        # strategy performance without commission
        prices[f'{signal}_strat_daily_perf'] = prices['returns']*prices[f'{signal}_strategy']

        prices[f'{signal}_strat_perf_cum'] = np.cumprod(prices[f'{signal}_strat_daily_perf'] + 1) - 1

        return prices[[f'{signal}_strat_daily_perf', f'{signal}_strat_perf_cum']]

    def run_perf_w_comms(self, signal, prices=None, comms=0.01, asset_p=None):

        """Method to calculate cumulative return of a strategy for a specific signal taking into account 1% commissions.
        Requires the signal input. Takes a DataFrame as input. It creates a lon only strategy using the previous method.
        :param prices: DataFrame containing High, Low and close prices for a security as well as signal
        :param signal: series containing a set of 1,0,-1 values indicating the signal
        :param asset_p: column name of prices tseries
        :param comms: float containing the commission in % terms (1% = 0.01)"""

        asset_p = self.asset_p if asset_p is None else asset_p

        prices = self.df.reset_index() if prices is None else prices
        prices = prices.copy(deep=True)

        prices['returns'] = prices[asset_p].pct_change()

        if f'{signal}_strategy' not in prices.columns:

            prices[f'{signal}_strategy'] = self.long_only_strategy(signal=signal)

        else:
            pass

        # strategy performance with commission
        prices[f'{signal}_strategy(-1)'] = prices[f'{signal}_strategy'].shift(1)
        prices[f'{signal}_strat_comms'] = prices[f'{signal}_strategy']
        commission = 0

        for i, r in enumerate(prices.iterrows()):
            if (r[1][signal] == 1 or r[1][signal] == -1) and r[1][f'{signal}_strategy'] != r[1][f'{signal}_strategy(-1)']:
                commission = comms
            else:
                commission = 0.00
            prices.iloc[i, prices.columns.get_loc(f"{signal}_strat_comms")] = commission

        prices[f'{signal}_strat_daily_perf_comms'] = (prices['returns']-prices[f"{signal}_strat_comms"]) * prices[f'{signal}_strategy']

        prices[f'{signal}_strat_perf_comms_cum'] = np.cumprod(prices[f'{signal}_strat_daily_perf_comms'] + 1) - 1

        return prices[[f'{signal}_strat_daily_perf_comms',f'{signal}_strat_perf_comms_cum']]

    def buy_and_hold(self, prices=None, asset_p=None):

        """Method to calculate cumulative return of a buy and hold strategy.
        :param prices: DataFrame containing High, Low and close prices for a security
        :param asset_p: column of df containing price timeseries for the asset"""

        asset_p = self.asset_p if asset_p is None else asset_p

        prices = self.df.reset_index() if prices is None else prices
        prices = prices.copy(deep=True)

        prices['buy_and_hold_strat_daily_perf'] = prices[asset_p].pct_change()
        prices['buy_and_hold_strat_perf_cum'] = np.cumprod(prices['buy_and_hold_strat_daily_perf'] + 1) - 1

        return prices[['buy_and_hold_strat_daily_perf', 'buy_and_hold_strat_perf_cum']]

    def ann_ret(self, prices=None, measure=None, ann_factor=252):

        """Method to calculate annualised return of a strategy.
        :param prices: DataFrame containing returns series of the strategy
        :param measure: column of df containing cumulative returns series of the strategy
        :param ann_factor: parameter representing the annualisation factor"""

        prices = self.df.reset_index() if prices is None else prices
        prices = prices.copy(deep=True)

        if len(prices[measure]) < ann_factor:

            ann_ret = ((1 + prices[measure].iloc[-1]) ** (len(prices[measure]) / ann_factor)) - 1

        else:

            ann_ret = ((1 + prices[measure].iloc[-1]) ** (ann_factor / len(prices[measure]))) - 1

        return ann_ret

    def ann_vol(self, prices=None, measure=None, ann_factor=252):

        """Method to calculate annualised volatility of a strategy.
        :param prices: DataFrame containing returns series of the strategy
        :param measure: column of df containing single period returns series of the strategy
        :param ann_factor: parameter representing the annualisation factor"""

        prices = self.df.reset_index() if prices is None else prices
        prices = prices.copy(deep=True)

        ann_vol = np.std(prices[measure]) * (ann_factor ** 0.5)

        return ann_vol

    def ann_sharpe(self, ann_ret=None, ann_vol=None, rf=0.00):

        """Method to calculate annualised sharpe ration given ann ret and ann vol.
        :param ann_ret: annualised return for the period
        :param ann_vol: annualised vol for the period
        :param rf: risk-free rate used"""

        sharpe = (ann_ret-rf) / ann_vol

        return sharpe

    def stats_table(self, ann_ret=None, ann_vol=None, sharpe=None, strategy_name='Strategy'):

        """Method to generate table with results.
        :param ann_ret: annualised return for the period
        :param ann_vol: annualised vol for the period
        :param sharpe: ann sharpe for the period
        :param strategy_name: name of the streategy we are calculating perf for"""

        data = {'Ann. Return': ann_ret, 'Ann. Volatility': ann_vol, 'Ann. Sharpe': sharpe}
        results_table = pd.DataFrame.from_dict(data, orient='index', columns=[strategy_name])

        return results_table
